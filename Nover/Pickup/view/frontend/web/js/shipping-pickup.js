define([
    'jquery',
    'underscore',
    'ko',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/action/set-shipping-information',
    'Magento_Ui/js/modal/modal',
    'Magento_Checkout/js/model/checkout-data-resolver',
    'Magento_Checkout/js/checkout-data',
    'mage/translate',
    'mage/validation'
], function (
    $,
    _,
    ko,
    customer,
    quote,
    selectShippingMethodAction,
    setShippingInformationAction,
    modal,
    checkoutDataResolver,
    checkoutData,
    $t
) {
    'use strict';
    var pickupFormPopUp = null;

    var pickup = {
        defaults: {
            template: 'Nover_Pickup/shipping',
            pickupFormTemplate: 'Nover_Pickup/shipping-pickup/form',
            pickupForm: {
                options: {
                    title: "Select Store Address"
                },
                element: '#opc-form-pickup-shipping-address'
            },
            storePickup: {}
        },

        storeName: ko.observable(),
        storeAddress1: ko.observable(),
        storeAddress2: ko.observable(),
        storeCity: ko.observable(),
        storeCountry: ko.observable(),
        storeZipcode: ko.observable(),

        isPickupFormPopUpVisible: ko.observable(false),

        showPickup: ko.computed(function () {
            if (!quote.shippingMethod()) {
                return false;
            }

            return (quote.shippingMethod()['carrier_code'] === 'pickup');
        }),

        initialize: function () {
            var self = this;
            this._super();
            this.isPickupFormPopUpVisible.subscribe(function (value) {
                if (value) {
                    self.getPickupFormPopUp().openModal();
                }
            });
        },

        /**
         * @return {*}
         */
        getPickupFormPopUp: function () {
            var self = this;

            if (!pickupFormPopUp) {

                this.pickupForm.options.buttons = [
                    {
                        text: $t('Save Store'),
                        class: 'action primary action-save-address',
                        click: self.saveStoreAddress.bind(self)
                    },
                    {
                        text: $t('Cancel'),
                        class: 'action secondary action-hide-popup',
                        /** @inheritdoc */
                        click: this.onClosePickupFormPopUp.bind(this)
                    }
                ];

                // /** @inheritdoc */
                this.pickupForm.options.closed = function () {
                    self.isPickupFormPopUpVisible(false);
                };

                this.pickupForm.options.modalCloseBtnHandler = this.onClosePickupFormPopUp.bind(this);
                this.pickupForm.options.keyEventHandlers = {
                    escapeKey: this.onClosePickupFormPopUp.bind(this)
                };

                /** @inheritdoc */
                this.pickupForm.options.opened = function () {
                    // Store temporary address for revert action in case when user click cancel action
                    self.temporaryStoreAddress = $.extend(true, {}, self.getPickupStoreData());
                };

                pickupFormPopUp = modal(this.pickupForm.options, $(this.pickupForm.element));
            }

            return pickupFormPopUp;
        },

        saveStoreAddress: function () {
            var form = '#co-shipping-pickup-form';
            if ($(form).validation() && $(form).validation('isValid')) {
                quote.storeAddress = this.getPickupStoreAddress();
                this.getPickupFormPopUp().closeModal();
            }
        },

        getPickupStoreData: function () {
            return {
                'name' : this.storeName(),
                'address1' : this.storeAddress1(),
                'address2' : this.storeAddress2(),
                'city' : this.storeCity(),
                'country' : this.storeCountry(),
                'zipcode' : this.storeZipcode()
            }
        },

        /**
         * Show address form popup
         */
        showPickupFormPopUp: function () {
            this.isPickupFormPopUpVisible(true);
        },

        /**
         * Revert address and close modal.
         */
        onClosePickupFormPopUp: function () {
            this.revertOldStoreData();
            this.getPickupFormPopUp().closeModal();
        },

        /**
         *
         */
        revertOldStoreData: function () {
            this.storeName(this.temporaryStoreAddress.name);
            this.storeAddress1(this.temporaryStoreAddress.address1);
            this.storeAddress2(this.temporaryStoreAddress.address2);
            this.storeCity(this.temporaryStoreAddress.city);
            this.storeCountry(this.temporaryStoreAddress.country);
            this.storeZipcode(this.temporaryStoreAddress.zipcode);
        },

        /**
         *
         * @param {Object} shippingMethod
         * @returns {boolean}
         */
        selectStorePickup: function (shippingMethod) {
            selectShippingMethodAction(shippingMethod);
            checkoutData.setSelectedShippingRate(shippingMethod['carrier_code'] + '_' + shippingMethod['method_code']);
            return true;
        },

        /**
         * @param {Object} shippingMethod
         * @return {Boolean}
         */
        selectShippingMethod: function (shippingMethod) {
            selectShippingMethodAction(shippingMethod);
            checkoutData.setSelectedShippingRate(shippingMethod['carrier_code'] + '_' + shippingMethod['method_code']);
            return true;
        },

        getPickupStoreAddress: function () {
            return this.storeName() + ',\r\n'
                + this.storeAddress1() + ',\r\n'
                + this.storeAddress2() + ',\r\n'
                + this.storeCity() + ',\r\n'
                + this.storeCountry() + ',\r\n'
                + this.storeZipcode() + ',\r\n';
        },

        validateShippingInformation: function () {
            if(quote.shippingMethod().carrier_code === 'pickup') {
                if (!quote.storeAddress) {
                    this.errorValidationMessage('Please provide store you prefer to pick your order.');
                    return false;
                }
            }
            return this._super();
        }
    };

    return function (shipping) {
        return shipping.extend(pickup);
    };
});