/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'underscore',
    'ko',
    'Magento_Checkout/js/model/quote'
], function (
    $,
    _,
    ko,
    quote
) {
    'use strict';

    return function (payload) {
        if (quote.shippingMethod()['method_code'] === 'pickup'){
            payload.addressInformation['extension_attributes'] = {
                'store_pickup' : quote.storeAddress
            };
        } else {
            payload.addressInformation['extension_attributes'] = {};
        }

        return payload;
    };
});
