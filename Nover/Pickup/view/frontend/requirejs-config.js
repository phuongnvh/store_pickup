var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Nover_Pickup/js/shipping-pickup': true
            }
        }
    },

    map: {
        '*': {
            'Magento_Checkout/js/model/shipping-save-processor/payload-extender': 'Nover_Pickup/js/model/shipping-save-processor/payload-extender'
        }
    }
};