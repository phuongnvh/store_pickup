<?php

namespace Nover\Pickup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Setup\Module\I18n\Parser\Adapter\Php;

/**
 * Class DataAssignObserver
 * @package Nover\Pickup\Observer
 */
class StorePickupObserver implements ObserverInterface
{
    const STORE_PICKUP_SHIPPING_METHOD_CODE = 'pickup_pickup';
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote  $quote */
        $quote = $observer->getQuote();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();

        if ($order->getShippingMethod() == self::STORE_PICKUP_SHIPPING_METHOD_CODE && $quote->getExtShippingInfo()) {
            $shippingDescription = $order->getShippingDescription() . PHP_EOL;
            $shippingDescription .= "Store: " .  $quote->getExtShippingInfo();
            $order->setShippingDescription($shippingDescription);
        }

        return $this;
    }
}