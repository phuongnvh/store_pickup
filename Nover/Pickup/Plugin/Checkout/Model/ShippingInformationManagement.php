<?php
/**
 *
 */
namespace Nover\Pickup\Plugin\Checkout\Model;

/**
 * Class ShippingInformationManagement
 * @package Nover\Pickup\Plugin\Checkout\Model
 */
class ShippingInformationManagement
{
    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * ShippingInformationManagement constructor.
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     */
    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }
    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $addressExtAttributes = $addressInformation->getExtensionAttributes();
        if (!empty($storePickup = $addressExtAttributes->getStorePickup())) {
            $quote = $this->quoteRepository->getActive($cartId);
            $quote->setExtShippingInfo($storePickup);
        }
    }
}